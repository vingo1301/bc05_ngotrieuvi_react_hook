import React,{useState} from 'react'
import { dataGlasses } from '../dataGlasses/dataGlasses'
import RenderChoiceGlass from './RenderChoiceGlass';

export default function Ex_glass() {
    let [isRender,setIsRender] = useState(false);
    let [glass,setGlass] = useState({});
    let handleChangeGlass = (item) => {
        setGlass(item);
        setIsRender(true);
    }
    let renderGlasses = () => {
        return dataGlasses.map((item) => {
            return <button onClick={() => handleChangeGlass(item)} className='btn col-2'><img src={item.src} alt="" /></button>
        })
    }
  return (
    <div className='container h-100'>
        <div className='model'>
            <img src="./img/model.jpg" alt="" />
            {isRender && <RenderChoiceGlass glass = {glass}></RenderChoiceGlass>}
        </div>
        <div className='glassesList row'>
            {renderGlasses()}
        </div>
    </div>
  )
}
