import React from 'react'

export default function RenderChoiceGlass(props) {
  return (
    <div>
        <div className='detail'>
                <h4>{props.glass.name}</h4>
                <span>{props.glass.price}$</span>
                <p>{props.glass.description}</p>
        </div>
        
        <img className='glass' src={props.glass.virtualImg} alt="" />
        
    </div>
  )
}
